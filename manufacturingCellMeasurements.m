model_name = 'manufacturingCell';

%% Each time unit represents ten seconds, mandating 100 seconds to produce each material unit.
t = 8640; % 86400s = 1 day
t_intermediate = 6120; % 17hr
M = t/10;
n_simulations = 1000;

%% Define simulation types
AttackType = ["no attack"; "data_corruption"; "data_modification";"loaded"; "unloaded"];

%% Results
processed_mat_day = zeros(length(AttackType),n_simulations);
processed_mat_intermediate = zeros(length(AttackType),n_simulations);
WFM = zeros(length(AttackType),n_simulations);
WFI = zeros(length(AttackType),n_simulations);
WFR = zeros(length(AttackType),n_simulations);
total_delay_day = zeros(length(AttackType),n_simulations);
total_delay_intermediate = zeros(length(AttackType),n_simulations);

for j =1:length(AttackType) %For each attack type
    strat = j + 3;
    for i = 1:n_simulations
        seed = randi(600);
        out = sim(model_name, t); % run simmulation  
        % Stalled states
        WFM(j, i) = any(out.WFMScopeData.signals.values);
        WFI(j, i) = any(out.WFIScopeData.signals.values);
        WFR(j, i) = any(out.WFRScopeData.signals.values);
        % Sum time spent in stalled states
        combined_stalled_states = out.WFMScopeData.signals.values + out.WFIScopeData.signals.values + out.WFRScopeData.signals.values;
        total_delay_day(j, i) = nnz(combined_stalled_states)/12;
        total_delay_intermediate(j, i) = nnz(combined_stalled_states(1:t_intermediate))/12;
        % Processed material
        intermediate_index = find(out.totalScopeData.time<=t_intermediate, 1, 'last');          % Find the last time slice before t_intermediate
        if intermediate_index
            processed_mat_intermediate(j, i) = out.totalScopeData.signals.values(intermediate_index);  %number of processed materials before t_intermediate
        end
        if out.totalScopeData.signals.values
            processed_mat_day(j, i) = out.totalScopeData.signals.values(end);                          %total number of processed materials
        end

    end   
end

%% Results %%
ForcedWFM = zeros(length(AttackType), 1);
ForcedWFI = zeros(length(AttackType), 1);
ForcedWFR = zeros(length(AttackType), 1);
ProbOfReachingDailyMaterials = zeros(length(AttackType), 1);
ProbOfReachingDailyMaterialsEarly = zeros(length(AttackType), 1);
AvgDelayEarly = zeros(length(AttackType), 1);
AvgMaterialsEarly = zeros(length(AttackType), 1);
AvgDelayinDay = zeros(length(AttackType), 1);
AvgMaterialsinDay = zeros(length(AttackType), 1);

for j = 1:length(AttackType)
    ForcedWFM(j) = any(WFM(j));
    ForcedWFI(j) = any(WFI(j));
    ForcedWFR(j) = any(WFR(j));
    ProbOfReachingDailyMaterials(j) = nnz(processed_mat_day(j, :)>=M) / length(processed_mat_day(j, :)); % probability of M
    ProbOfReachingDailyMaterialsEarly(j) = nnz(processed_mat_intermediate(j, :)>=M) / length(processed_mat_intermediate(j, :)); % probability of M

    % Intermediate
    AvgDelayEarly(j) = mean(total_delay_intermediate(j, :));
    AvgMaterialsEarly(j) = mean(processed_mat_intermediate(j, :));
    
    % In Day
    AvgDelayinDay(j) = mean(total_delay_day(j, :));
    AvgMaterialsinDay(j) = mean(processed_mat_day(j, :));
end

table(AttackType, ForcedWFM, ForcedWFI, ForcedWFR, AvgDelayEarly, AvgDelayinDay, AvgMaterialsEarly, AvgMaterialsinDay, ProbOfReachingDailyMaterials, ProbOfReachingDailyMaterialsEarly)