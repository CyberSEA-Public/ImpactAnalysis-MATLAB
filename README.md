# A Generalized Impact Analysis Approach for Evaluating Dependable Industrial Control Systems
This repository contains files used to model a manufacturing cell control system (MCCS) using MATLAB/Simulink and perform experiments with different tampering and spoofing attacks described in an eponymous research paper.

## Table of contents
* [Description](#description)
* [Technologies](#technologies)
* [Files](#files)
* [Setup](#setup)

### Description
This project is an effort towards using discrete-event modeling and simulation to analyze the impact of cyberattacks on industrial control systems by modeling attackers and the system as models created in MATLAB/Simulink and the SimEvents library.
	
### Technologies
Project is created with:
* [MATLAB R2023a](https://www.mathworks.com/products/matlab.html)

### Files
* __manufacturingCell.slx__: The model file that contains all the described system agents and attackers.
* __manufacturingCellMeasurements.m__: Script file containing all the queries for model analysis.
	
### Setup
To run this project, [download](https://www.mathworks.com/help/install/ug/install-products-with-internet-connection.html) and install MATLAB R2023a.
1. Open the model file (manufacturingCell.slx) in MATLAB
2. Run the script
